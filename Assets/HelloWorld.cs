using UnityEngine;

public class HelloWorld : MonoBehaviour
{
    private int _count = 0;
    
    void OnGUI()
    {
        GUILayout.Label($"Hello, Demo {_count}!");
    }
    
    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            _count += 7;
        }
    }
}
